# A quoi sert et comment utiliser ce repo ?

L'objectif de ce repo est de pouvoir gérer l'accès, la maintenance et l'historisation des statuts de MIAGE Connection.

Dans une optique d'évolutivité, nous pourrons aussi mettre en place un process de proposition d'une motion de modification via une merge request. 

## Consulter les documents

Toutes les dernières versions des documents sont disponibles en pdf dans le dossier `pdfs`. 

Vous pouvez aussi consulter les documents directement en markdown et consulter toutes les modifications en cliquant sur le fichier markdown puis en allant dans l'historique.  


## Proposer une modification des statuts et RI

Voici la marche à suivre pour proposer une modification des statuts/RI :

1. Fork le projet

https://gitlab.com/pbelabbes/mc_status/forks/new

1. Créer la branche

 Créer une branche à partir de la branche **master** en la nommant comme suit : `STATUTS/RI-DATE-Event-OBJECT_MOTION-SIGLE`. 
 
 *Si je souhaite par exemple faire une modification de *l'article 1* du RI pour modifier la définition de la *Fédération* dans le cadre du Spring je vais créer la branche : `RI-05/05/2019-Spring-Modification_Définition_Fédération-MDF`*

2. Appliquer des modifications

Faire les modifications sur la branche précédement créée. Chaque commit doit avec pour titre : `[DATE] - Event : SIGLE MOTION - Article modifié - description de la modification`

Dans le cas de notre exemple, nous faisons la modification de la définition de *Fédération* dans *l'article 1* du **RI**. Après quoi nous enregistrons notre modification via le commit suivant : `05/05/2019-Spring : ATE - Article 1 - modification de la définition de Fédération en modifiant le nombre d'associations adhérentes nécessaire pour être considéré comme une fédération de 5 à 10`

3. Envoie de la motion

Une fois que toutes les modifications apportées par la motion sont effectuées. Vous pouvez faire une Merge Request sur la branche `Validation`. 

La description de la merge request doit être de la forme : 
```
STATUTS/RI
DATE - EVENT
Association : 
Soutien :
OBJET MOTION : 
SIGLE :

DESCRIPTION :
```

Avec notre exemple cela donnerait : 

```
RI
05/05/2019 - Spring
Association : BDE MIAGE Montargis
Soutien : BDE MIAGE Voiron
OBJET MOTION : Modification Définition Fédération
SIGLE :  MDF

DESCRIPTION : Actualisation du nombre d'asso nécéssaire pour la définition de Fédération. 
```